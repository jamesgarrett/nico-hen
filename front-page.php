@layout('layouts.one-column')



@section('main')

	<div class="grid">



		@wpquery(array('post_type'=>'projects'))

		    <? 

			$project_type = get_field('project_type');

		    $thumb = get_field('thumbnail'); 

		    $height =$thumb['height']

		    ?>

		    <a class="grid--box" href="{{the_permalink()}}" style="width:100%;background-size:100% auto;vertical-align:middle;margin-bottom:15px;max-height:{{ $thumb['height'] }}px;min-height:475px;background-image:url({{ $thumb['url'] }});">

		    	<div class="meta--inner">

		    		<ul class="box--meta">

		    			<li><strong>Project: </strong>{{the_title()}}</li>

		    			<li><strong>Type: </strong>{{$project_type}}</li>

		    			<li><em>{{the_excerpt()}}</em></li>

			    	</ul>

				</div>

		    </a>

		@wpempty

		    <li>{{ __('Sorry, no posts matched your criteria.') }}</li>

		@wpend





	</div>

@endsection