@layout('layouts.master')

{{--
{{-- Template Name: Project
{{--}}

@section('main')
	<? $acf_slider = array(); ?>
	<div class="grid">
	
		@wpposts
		    <?
		    $project_content = get_field('project_content');
		    $project_type = get_field('project_type');
		    array_push($acf_slider, $project_content);
		    ?>
			@if($project_type=='Still')
				@include('partials.flex-slider')
				@include('partials.project-still')
			@if($project_type=='Motion')
				@include('partials.project-motion')
			@endif	
		@wpempty
		    <li>{{ __('Sorry, no posts matched your criteria.') }}</li>
		@wpend

	</div>
	@include('partials.acf-slider',array('acf_slider'=>$acf_slider))
@endsection
