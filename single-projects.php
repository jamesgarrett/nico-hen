@layout('layouts.archive')

@section('main')
	<? $acf_slider = array(); ?>
	<div class="grid">
	
		@wpposts
		    <?
		    $project_content = get_field('project_content');
		    $project_type = get_field('project_type');
		    array_push($acf_slider, $project_content);
		    ?>
			@if($project_type=='Motion')
				@include('partials.project-motion')
			@else
				@include('partials.project-still',array($i))
		    @endif
		@wpempty
		    <li>{{ __('Sorry, no posts matched your criteria.') }}</li>
		@wpend

	</div>
	@include('partials.acf-slider',array('acf_slider'=>$acf_slider))
@endsection