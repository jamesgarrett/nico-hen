@if( $project_content )

    <a class="project-title" name="{{the_title();}}" href="#slide-{{$i}}">

        <div style="background-color:{{$color;}}">{{the_title();}}</div>

    </a>

    <div class="action-bar">

        <!--<button href="#slide-" class="open-slider-icon">View Gallery</button><a class="layout-option-icon" style="display:none;"><i class="fa fa-th"></i></a>-->

    </div>

    <ul>

        <? $i = 0; ?> 

        @foreach ($project_content as $photo)

            <? $i = $i+1; $width = $photo['width']; $height = $photo['height']; ?>

            

                <? if($width > $height): ?> <a class="grid--box" href="{{$photo['url']}}" style="width="100%;">

                <? else: ?><a class="grid--box" href="{{$photo['url']}}" style="width:50%;text-align:center;margin:0 auto;">

                <? endif; ?>

                 <img src="{{$photo['url']}}" style="height:auto;max-width:100%;vertical-align:center;">

                <ul class="box--meta">

                    <li class="meta--inner" style="text-align: center;vertical-align: baseline;"><div>{{$photo['title'];}}</div></li>

                </ul>

            </a>

        <? endforeach; ?>

    </ul>

<? endif; ?>