@if(isset($acf_slider))

	<div class="acf_slider-fullscreen">
		<div class="acf_slider-fullscreen-inner">
			<div class="acf_slider" data-acf-slider='{{json_encode($acf_slider)}}'></div>
		</div>
		<button class="acf_slider-fullscreen--close" data-close>Close</button>
	</div>

@endif