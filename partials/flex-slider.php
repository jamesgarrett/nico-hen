<?php 

if( $project_content ): ?>
    <div id="slider" class="flexslider">
        <ul class="slides">
            @foreach ($project_content as $photo)
                <li class="grid--box">
                    <img src="<?php echo $photo['url']; ?>" alt="<?php echo $photo['alt']; ?>" />
                    <p><?php echo $photo['caption']; ?></p>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div id="carousel" class="flexslider">
        <ul class="slides">
            @foreach ($project_content as $photo)
                <li>
                    <img src="<?php echo $photo['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>