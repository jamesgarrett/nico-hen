<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,height=device-height">
<title>{{ bloginfo('name') }}</title>
<link rel="stylesheet" href="{{ get_stylesheet_uri() }}">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
{{ wp_head() }}