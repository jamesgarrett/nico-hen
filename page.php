@layout('layouts.one-column')

@section('main')

	@wpposts
	<div class="grid" style="width:400px;margin:35px auto 0 auto;line-height:1.5rem;">
		<? the_content(); ?>
	</div>

	@wpempty <li>{{ __('Sorry, no posts matched your criteria.') }}</li>
	@wpend


@endsection