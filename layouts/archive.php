<!DOCTYPE html>
<html lang="en">
<head>
	@include('partials.meta-head')
</head>
<body {{body_class()}}>
	 <header id="top" class="top--menu" style="position:absolute;">
	    	@include('partials.header')
	</header>
	<div id="app">
	    <main id="right" class="app--col">
	    	@yield('main')
	    	@include('partials.sidebar')
	    </main>
    </div>
    <footer>
	@include('partials.meta-foot')
	</footer>
</body>
</html>

