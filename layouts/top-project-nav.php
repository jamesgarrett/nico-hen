<!DOCTYPE html>
<html lang="en">
<head>
	@include('partials.meta-head')
</head>
<body {{body_class()}}>
	 <header id="top" class="top--menu">
	    	@include('partials.header')
	    	@include('partials.sidebar')
	</header>
	<div id="app">
	    <main class="app--col col-right">
	    	@yield('main')
	    </main>
    </div>
    <footer>
		@include('partials.meta-foot')
	</footer>
</body>
</html>

