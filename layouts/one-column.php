<!DOCTYPE html>

<html lang="en">

<head>

	@include('partials.meta-head')

</head>

<body {{body_class()}}>

	 <header id="top" class="top--menu">

	    	@include('partials.header')

	</header>

	<div id="app" style="margin-bottom:60px;">

	    <!-- <main id="right" class="app-\-col" style="width:100%;float:none;"> -->

	    	

	    <main class="app--col">

	    	@yield('main')

	    </main>

    </div>

    <footer style="position:relative;bottom:0;">

	@include('partials.meta-foot')

	</footer>

</body>

</html>



